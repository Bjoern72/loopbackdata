package loopback;

public enum ScaleUnit {

    DAILY,
    WEEKLY,
    MONTHLY
}
