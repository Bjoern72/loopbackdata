package loopback;

import archive.StockOut;
import lombok.Getter;
import lombok.Setter;
import model.PlotableSetImpl;
import model.TableDataRow;

import java.time.LocalDate;
import java.time.temporal.WeekFields;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;
import java.util.OptionalDouble;

@Getter
@Setter
public class LoopBackData<E extends Stock> extends ArrayList<Stock> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -524486822796805446L;
	LoopBackData<Stock> weekly ;
	LoopBackData<Stock> monthly;
	LoopBackData<Stock> daily;

	LoopBackData<Stock> stockList;

	LoopBackData<Stock> stocks;

	PlotableSetImpl plotableSet = new PlotableSetImpl();

	private ScaleUnit scaleUnit;

	public void setScaleUnit(ScaleUnit scaleUnit){

		this.scaleUnit = scaleUnit;

	}

	public LoopBackData<Stock> getData(){



		return stocks;

	}



	public LoopBackData<Stock> getDailyData(){

		return stocks;

	}

	public LoopBackData<Stock> getWeeklyData(){

		LoopBackData<Stock> weeklyData = initWeeklyData();

		initBasicRows();

		return initWeeklyData();
	}

	public void initBasicRows() {

		stocks = (LoopBackData<Stock>) this.clone();
		stocks = (LoopBackData<Stock>) this.clone();

		TableDataRow tdOpenRow = new TableDataRow();
		tdOpenRow.addData("open", getOpenAsDouble() );

		TableDataRow tdCloseRow = new TableDataRow();
		tdCloseRow.addData("close", getCloseAsDouble() );

		TableDataRow tdHighRow = new TableDataRow();
		tdHighRow.addData("high", getHighAsDouble() );

		TableDataRow tdLowRow = new TableDataRow();
		tdLowRow.addData("low", getLowAsDouble() );

		TableDataRow tdVolumeRow = new TableDataRow();
		tdVolumeRow.addData("volume", getVolumeAsDouble() );

		plotableSet.add(tdOpenRow);
		plotableSet.add(tdCloseRow);
		plotableSet.add(tdHighRow);
		plotableSet.add(tdLowRow);
		plotableSet.add(tdVolumeRow);
	}

	private LoopBackData<Stock> initWeeklyData()
	{

		LoopBackData<Stock> weekly = new LoopBackData<>();
		LocalDate date2 = this.get(0).getDate();

		//date2.setLenient(false);
	    double open = 0;
	    double high = 0;
	    double low = 0;
	    double close = 0;
	    double volume = 0;
		Stock weeklyVal;
		Stock dailyVal;
		int averageCount = 0;

		weekly = new LoopBackData<Stock>();
		for (Stock stock : stockList)
		{
			LocalDate date = stock.getDate();

			System.out.println("date " + date);
			WeekFields weekFields = WeekFields.of(Locale.getDefault());
			if(date2.get(weekFields.weekOfWeekBasedYear()) == date.get(weekFields.weekOfWeekBasedYear()))
			{
				open += stock.getOpen();
				high += stock.getHigh();
				low += stock.getLow();
				close += stock.getClose();
				volume+= stock.getVolume();
				averageCount++;
			}
			else
			{
				weeklyVal = new Stock();
				weeklyVal.setDate(date2);
				weeklyVal.setOpen(open/averageCount);
				weeklyVal.setHigh(high/averageCount);
				weeklyVal.setLow(low/averageCount);
				weeklyVal.setClose(close/averageCount);
				weeklyVal.setVolume(volume/averageCount);
				
				weekly.add(weeklyVal);
				date2 = date;
			    open = 0;
			    high = 0;
			    low = 0;
			    close = 0;
			    volume = 0;
			    weeklyVal = null;
			    averageCount = 0;
			    
				open += stock.getOpen();
				high += stock.getHigh();
				low += stock.getLow();
				close += stock.getClose();
				volume+= stock.getVolume();
				averageCount++;
			}
		}
		return weekly;
	}

	public LoopBackData<Stock> getMonthlyData(){

		initBasicRows();

		return this.initMonthlyData();
	}

	public LoopBackData<Stock> initMonthlyData()
	{
		LocalDate date2 = this.get(0).getDate();

		//date2.setLenient(false);
	    double open = 0;
	    double high = 0;
	    double low = 0;
	    double close = 0;
	    double volume = 0;
		Stock weeklyVal;
		Stock monthlyVal;
		Stock dailyVal;
		int averageCount = 0;

		monthly = new LoopBackData<Stock>();
		for (Stock stock : stockList)
		{
			LocalDate date = stock.getDate();

			if(date2.getMonthValue()==date.getMonthValue())
			{
				open += stock.getOpen();
				high += stock.getHigh();
				low += stock.getLow();
				close += stock.getClose();
				volume+= stock.getVolume();
				averageCount++;
			}
			else
			{
				monthlyVal = new Stock();
				monthlyVal.setDate(date2);
				monthlyVal.setOpen(open/averageCount);
				monthlyVal.setHigh(high/averageCount);
				monthlyVal.setLow(low/averageCount);
				monthlyVal.setClose(close/averageCount);
				monthlyVal.setVolume(volume/averageCount);
				
				monthly.add(monthlyVal);
				date2 = date;
			    open = 0;
			    high = 0;
			    low = 0;
			    close = 0;
			    volume = 0;
			    weeklyVal = null;
			    averageCount = 0;
			    
				open += stock.getOpen();
				high += stock.getHigh();
				low += stock.getLow();
				close += stock.getClose();
				volume+= stock.getVolume();
				averageCount++;
			}
		}
		return monthly;
	}
	
	public String toString()
	{
		
		StringBuffer strBuf = new StringBuffer("[");
		
		strBuf.append("[Date, Open, High, Low, Close, Volume, Adj. Close*]\n");
		for(Stock stock : this)
		{
			strBuf.append("[");
			strBuf.append(stock.getDate()+", ");
			strBuf.append(stock.getOpen()+ ", ");
			strBuf.append(stock.getHigh()+ ", ");
			strBuf.append(stock.getLow()+ ", ");
			strBuf.append(stock.getClose());
			
			strBuf.append(stock.getVolume());
			//strBuf.append(stock.getAdjClose());
			strBuf.append("]\n");
			
		}
		strBuf.append("]");
		return strBuf.toString();
	}


	private ArrayList<Double> getOpenAsDouble(){
		ArrayList<Double> allOpenings = new ArrayList<Double>();
		int i = 0;
		for(Stock stock : stocks){
			allOpenings.add(i, this.get(i).getOpen());
			i++;
		}
		return allOpenings;
	}

	private ArrayList<Double> getHighAsDouble(){
		ArrayList<Double> allhighs = new ArrayList<Double>();
		int i = 0;
		for(Stock stock : stocks){
			allhighs.add(i, this.get(i).getHigh());
			i++;
		}
		return allhighs;
	}

	private ArrayList<Double> getLowAsDouble(){
		ArrayList<Double> allLows = new ArrayList<Double>();
		int i = 0;
		for(Stock stock : stocks){
			allLows.add(i, this.get(i).getLow());
			i++;
		}
		return allLows;
	}

	private ArrayList<Double> getCloseAsDouble(){
		ArrayList<Double> allCloses = new ArrayList<Double>();
		int i = 0;
		for(Stock stock : stocks){
			allCloses.add(i, this.get(i).getClose());
			i++;
		}
		return allCloses;
	}

	private ArrayList<Double> getVolumeAsDouble(){
		ArrayList<Double> allVolumes = new ArrayList<Double>();
		int i = 0;
		for(Stock stock : stocks){
			allVolumes.add(i, this.get(i).getVolume());
			i++;
		}
		return allVolumes;
	}



    public Double getHighestHigh(){

        //Optional<Double> max = this.getCloseAsDouble().stream().max( Double::max );

        OptionalDouble max = this.getCloseAsDouble().stream().mapToDouble(Double::doubleValue).max();

        return max.getAsDouble();
    }

    public Double getLowestLow(){

        OptionalDouble min = this.getCloseAsDouble().stream().mapToDouble(Double::doubleValue).min();

        return min.getAsDouble();

    }

    /**
     * Return the high or low of given trading day and, and return whichever is higher
     *
     * @param  stock
     * @return The max of two input v
     */
    private double getTrueLow(StockOut stock){

        return Math.min(stock.getHigh(), stock.getClose());

    }

    /**
     * Return the high or low of given day and, and return whichever is higher
     *
     * @param  stock
     * @return
     */
    private double getTrueHigh(StockOut stock){

        return Math.max(stock.getHigh(), stock.getClose());

    }

}
