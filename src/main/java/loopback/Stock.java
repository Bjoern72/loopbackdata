package loopback;

import org.apache.commons.lang.builder.ReflectionToStringBuilder;

import java.time.LocalDate;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class Stock {
    private String ticker;
    //private Calendar date;
    private LocalDate date;

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    private double open;
    private double high;
    private double low;
    private double close;
    private double volume;

    public Stock() { }
    public void setTicker(String aTicker) { ticker = aTicker; }
    public String getTicker() { return ticker; }

    public void setOpen(double aOpen) { open = aOpen; }
    public double getOpen() { return open; }
    public void setHigh(double aHigh) { high = aHigh; }
    public double getHigh() { return high; }
    public void setLow(double aLow) { low = aLow; }
    public double getLow() { return low; }
    public void setClose(double aClose) { close = aClose; }
    public double getClose() { return close; }
    public void setVolume(double aVolume) { volume = aVolume; }
    public double getVolume() { return volume; }

    //constructor for fast typing of ATR test values..
    public Stock(LocalDate date, double high, double low, double close) {

        this.date = date;
        this.high = high;
        this.low = low;
        this.close = close;
    }

    public double getDataValue(String dataType)
    {
    	
    	double value = 0.0d;
    	if(dataType.equals("CLOSE"))
    	{
    		value = getClose();
    	}
    	else if (dataType.equals("VOLUME"))
    	{
    		value = getVolume();
    	}
    	else if (dataType.equals("OPEN"))
    	{
    		value = getOpen();
    	}
    	else if (dataType.equals("HIGH"))
    	{
    		value = getHigh();
    	}
    	else if (dataType.equals("LOW"))
    	{
    		value = getLow();
    	}
		return value;
    }
    

    public String toString() {
        return ReflectionToStringBuilder.toString(this);
    }

}










