package archive;

import java.util.Calendar;
import java.util.LinkedList;
import java.util.OptionalDouble;


public class LoopBackDataOut<E extends StockOut> extends LinkedList<StockOut> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -524486822796805446L;
	LoopBackDataOut<StockOut> weekly ;
	LoopBackDataOut<StockOut> monthly;
	LoopBackDataOut<StockOut> daily;

	LoopBackDataOut<StockOut> stockList;
	
//	public LoopBackData(LoopBackData<Stock> stockList)
//	{
//		super();
//		this.stockList = stockList;
//	}

	public LoopBackDataOut<StockOut> getWeeklyData(LoopBackDataOut<StockOut> loopBack)
	{
		stockList = loopBack; 
		Calendar date2 = stockList.peek().getDate();
		date2.setLenient(false);
		
		//date2.setLenient(false);
	    double open = 0;
	    double high = 0;
	    double low = 0;
	    double close = 0;
	    double volume = 0;
		StockOut weeklyVal;
		StockOut dailyVal;
		int averageCount = 0;

		weekly = new LoopBackDataOut<StockOut>();
		for (StockOut stock : stockList)
		{
			Calendar date = stock.getDate();
			date.setLenient(false);
			
			System.out.println("date " + date);
			
			date.setLenient(false);
			//System.out.println(date2.get(Calendar.WEEK_OF_YEAR));
			//System.out.println(date.get(Calendar.WEEK_OF_YEAR));
			if(date2.get(Calendar.WEEK_OF_YEAR)==date.get(Calendar.WEEK_OF_YEAR))
			{

//				open += (stock.getOpen()).doubleValue();
//				high += (stock.getHigh()).doubleValue();
//				low += (stock.getLow()).doubleValue();
//				close += (stock.getClose()).doubleValue();
//				volume+= (stock.getVolume()).doubleValue();
//				averageCount++;

				open += stock.getOpen();
				high += stock.getHigh();
				low += stock.getLow();
				close += stock.getClose();
				volume+= stock.getVolume();
				averageCount++;
			
			
			}
			else
			{
				weeklyVal = new StockOut();
				weeklyVal.setDate(date2);

				//weeklyVal.setOpen(new BigDecimal(open/averageCount));
//				weeklyVal.setHigh(new BigDecimal(high/averageCount));
//				weeklyVal.setLow(new BigDecimal(low/averageCount));
//				weeklyVal.setClose(new BigDecimal(close/averageCount));
//				weeklyVal.setVolume(new BigDecimal(volume/averageCount));

				weeklyVal.setOpen(open/averageCount);
				weeklyVal.setHigh(high/averageCount);
				weeklyVal.setLow(low/averageCount);
				weeklyVal.setClose(close/averageCount);
				weeklyVal.setVolume(volume/averageCount);
				
				
				weekly.add(weeklyVal);
				date2 = date;
			    open = 0;
			    high = 0;
			    low = 0;
			    close = 0;
			    volume = 0;
			    weeklyVal = null;
			    averageCount = 0;
			    
				open += stock.getOpen();
				high += stock.getHigh();
				low += stock.getLow();
				close += stock.getClose();
				volume+= stock.getVolume();
				averageCount++;
			}
		}
		return weekly;
	}
	
	public LoopBackDataOut<StockOut> getMonthlyData(LoopBackDataOut<StockOut> loopBack)
	{
		stockList = loopBack; 
		Calendar date2 = stockList.peek().getDate();
		date2.setLenient(false);
		
		//date2.setLenient(false);
	    double open = 0;
	    double high = 0;
	    double low = 0;
	    double close = 0;
	    double volume = 0;
		StockOut weeklyVal;
		StockOut monthlyVal;
		StockOut dailyVal;
		int averageCount = 0;

		monthly = new LoopBackDataOut<StockOut>();
		for (StockOut stock : stockList)
		{
			Calendar date = stock.getDate();
			date.setLenient(false);
			
			System.out.println("date " + date);
			
			date.setLenient(false);
			//System.out.println(date2.get(Calendar.WEEK_OF_YEAR));
			//System.out.println(date.get(Calendar.WEEK_OF_YEAR));
			if(date2.get(Calendar.MONTH)==date.get(Calendar.MONTH))
			{
				open += stock.getOpen();
				high += stock.getHigh();
				low += stock.getLow();
				close += stock.getClose();
				volume+= stock.getVolume();
				averageCount++;
			}
			else
			{
				monthlyVal = new StockOut();
				monthlyVal.setDate(date2);
				monthlyVal.setOpen(open/averageCount);
				monthlyVal.setHigh(high/averageCount);
				monthlyVal.setLow(low/averageCount);
				monthlyVal.setClose(close/averageCount);
				monthlyVal.setVolume(volume/averageCount);
				
				monthly.add(monthlyVal);
				date2 = date;
			    open = 0;
			    high = 0;
			    low = 0;
			    close = 0;
			    volume = 0;
			    weeklyVal = null;
			    averageCount = 0;
			    
				open += stock.getOpen();
				high += stock.getHigh();
				low += stock.getLow();
				close += stock.getClose();
				volume+= stock.getVolume();
				averageCount++;
			}
		}
		return monthly;
	}
	
	public String toString()
	{
		
		StringBuffer strBuf = new StringBuffer("[");
		
		strBuf.append("[Date, Open, High, Low, Close, Volume, Adj. Close*]\n");
		for(StockOut stock : this)
		{
			strBuf.append("[");
			strBuf.append(stock.getDate()+", ");
			strBuf.append(stock.getOpen()+ ", ");
			strBuf.append(stock.getHigh()+ ", ");
			strBuf.append(stock.getLow()+ ", ");
			strBuf.append(stock.getClose());
			
			strBuf.append(stock.getVolume());
			//strBuf.append(stock.getAdjClose());
			strBuf.append("]\n");
			
		}
		strBuf.append("]");
		return strBuf.toString();
	}

    /**
     *
     * @param loopBack
     * @return
     */
	public static LinkedList<Double> getOpenAsDouble(LoopBackDataOut<StockOut> loopBack){
		LinkedList<Double> allOpenings = new LinkedList<Double>();
		int i = 0;
		for(StockOut stock : loopBack){
			allOpenings.add(i, loopBack.get(i).getOpen());
			i++;
		}
		return allOpenings;
	}

	public static LinkedList<Double> getHighAsDouble(LoopBackDataOut<StockOut> loopBack){
		LinkedList<Double> allhighs = new LinkedList<Double>();
		int i = 0;
		for(StockOut stock : loopBack){
			allhighs.add(i, loopBack.get(i).getHigh());
			i++;
		}
		return allhighs;
	}

	public static LinkedList<Double> getLowAsDouble(LoopBackDataOut<StockOut> loopBack){
		LinkedList<Double> allLows = new LinkedList<Double>();
		int i = 0;
		for(StockOut stock : loopBack){
			allLows.add(i, loopBack.get(i).getLow());
			i++;
		}
		return allLows;
	}

	public static LinkedList<Double> getCloseAsDouble(LoopBackDataOut<StockOut> loopBack){
		LinkedList<Double> allCloses = new LinkedList<Double>();
		int i = 0;
		for(StockOut stock : loopBack){
			allCloses.add(i, loopBack.get(i).getClose());
			i++;
		}
		return allCloses;
	}

	public static LinkedList<Double> getVolumeAsDouble(LoopBackDataOut<StockOut> loopBack){
		LinkedList<Double> allVolumes = new LinkedList<Double>();
		int i = 0;
		for(StockOut stock : loopBack){
			allVolumes.add(i, loopBack.get(i).getVolume());
			i++;
		}
		return allVolumes;
	}

    public LinkedList<Double> getCloseAsDouble(){
        LinkedList<Double> allCloses = new LinkedList<Double>();
        int i = 0;
        for(StockOut stock : this){
            allCloses.add(i, this.get(i).getClose());
            i++;
        }
        return allCloses;
    }


    public Double getHighestHigh(){

        //Optional<Double> max = this.getCloseAsDouble().stream().max( Double::max );

        OptionalDouble max = this.getCloseAsDouble().stream().mapToDouble(Double::doubleValue).max();

        return max.getAsDouble();
    }

    public Double getLowestLow(){

        OptionalDouble min = this.getCloseAsDouble().stream().mapToDouble(Double::doubleValue).min();

        return min.getAsDouble();

    }

    /**
     * Return the high or low of given trading day and, and return whichever is higher
     *
     * @param  stock
     * @return The max of two input v
     */
    private double getTrueLow(StockOut stock){

        return Math.min(stock.getHigh(), stock.getClose());

    }

    /**
     * Return the high or low of given day and, and return whichever is higher
     *
     * @param  stock
     * @return
     */
    private double getTrueHigh(StockOut stock){

        return Math.max(stock.getHigh(), stock.getClose());

    }

    /**
     * ATR (Average True Range)
     * <p></p>
     * Description taken from 'Getting started in technical analysis' by Jack D. Swagger', Wiley . 322
     * <P></P>
     * The range (R) of a given day is simply the high (H) minus the low (L): H - L = R.
     * <p></p>
     * The true range (TR), however, is defined as the 'true high' (TH) minus the true 'low' (TL): TH - TL = TR.
     * <p></p>
     * The
     * @return
     */
    public LinkedList<Double> getATR(){

        LinkedList<Double> atr = new LinkedList<>();

        LinkedList<StockOut> stockList = this.stockList;

        Double high;
        Double low;
        Double close;

        for(StockOut stock : stockList){




        }

        return atr;

    }




}
