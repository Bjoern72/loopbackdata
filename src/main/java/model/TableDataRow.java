package model;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;

@Getter
@Setter
public class TableDataRow {

    private String rowName;
    private ArrayList<Double> arrayList;
    public void addData(String rowName, ArrayList<Double> data){

        this.rowName = rowName;
        this.arrayList = data;

    }
}
