package model;


import com.google.common.collect.HashBasedTable;
import com.google.common.collect.ImmutableTable;
import com.google.common.collect.Table;

public class TestGuavaTable {


    public static void main(String[] args) {
        Table<String, String, Integer> weightedGraph = HashBasedTable.create();

        weightedGraph.put("v1", "v2", 4);
        weightedGraph.put("v1", "v3", 20);
        weightedGraph.put("v2", "v3", 5);

        weightedGraph.row("v1"); // returns a Map mapping v2 to 4, v3 to 20
        weightedGraph.column("v3"); // returns a Map mapping v1 to 20, v2 to 5

        Table<String, String, Integer> universityCourseSeatTable
                = HashBasedTable.create();
        universityCourseSeatTable.put("Mumbai", "Chemical", 120);
        universityCourseSeatTable.put("Mumbai", "IT", 60);
        universityCourseSeatTable.put("Harvard", "Electrical", 60);
        universityCourseSeatTable.put("Harvard", "IT", 120);

        Table<String, Integer, Double> bc = HashBasedTable.create();

        Table<String, String, Integer> universityCourseSeatTable2
                = ImmutableTable.<String, String, Integer> builder()
                .put("Mumbai", "Chemical", 120).build();

        universityCourseSeatTable.putAll(universityCourseSeatTable2);

    }


}
