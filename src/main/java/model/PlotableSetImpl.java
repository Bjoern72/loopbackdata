package model;

import com.google.common.collect.HashBasedTable;
import com.google.common.collect.ImmutableTable;
import com.google.common.collect.Table;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class PlotableSetImpl implements PlotableSet{

    private Table<Integer, String, Double> bc = HashBasedTable.create();

    private AtomicInteger atomicInteger = new AtomicInteger(0);

    @Override
    public void add(TableDataRow tableDataDecorator) {

        final String rowName = tableDataDecorator.getRowName();
        ArrayList<Double> plotableData = tableDataDecorator.getArrayList();

        //plotableData.stream().forEachOrdered( pd -> this.bc.put( atomicInteger.get(), rowName, pd.doubleValue() ));

        for( double d : plotableData){


            bc.put( atomicInteger.getAndIncrement(), rowName, d);
        }

        //ArrayList<Double> doubles = this.bc.column(rowName).values();
    }

    public  List<Double> getTableDataRow(String rowName){

        return new ArrayList<>(this.bc.column(rowName).values());

    }

}
